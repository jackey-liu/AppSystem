package com.bdqn.jackey.utils;

import java.util.HashMap;
import java.util.List;

/**
 * @author 刘俊伟 on 2020/10/15.
 */
public class LayData extends HashMap<String, Object> {
    /**
     * 设置layui数据要求格式
     * @param count 数量
     * @param data 数据
     * @return json对象
     */
    public static LayData getData(Integer count,List<?>data) {
        LayData map = new LayData();
        map.put("code", 0);
        map.put("msg", "success");
        map.put("count", count);
        map.put("data", data);
        return map;
    }

    /**
     * 上传文件
     * @param fileName 文件名回显
     * @return json对象
     */
    public static LayData getUploadFile(String fileName) {
        LayData map = new LayData();
        map.put("code", 0);
        map.put("msg", "success");
        map.put("fileName", fileName);
        return map;
    }
}
