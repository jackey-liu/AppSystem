package com.bdqn.jackey.interceptor;

import com.bdqn.jackey.entity.SysUser;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author 刘俊伟 on 2020/10/18.
 * 系统拦截器
 */
public class SystemInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        SysUser sysUser = (SysUser) session.getAttribute("sysUser");
        if (sysUser == null) {
            request.getRequestDispatcher("/WEB-INF/jsp/error/403.jsp").forward(request, response);
            return false;
        }
        return true;
    }
}
