package com.bdqn.jackey.service.impl;

import com.bdqn.jackey.mapper.AppVersionMapper;
import com.bdqn.jackey.service.AppVersionService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.bdqn.jackey.entity.AppVersion;
import java.util.List;

/**
 * @description：(AppVersion)表服务实现类
 * @author：jackey
 * @create：
 */
@Service("/appVersionService")
public class AppVersionServiceImpl implements AppVersionService {

    @Resource
    private AppVersionMapper appVersionMapper;

   /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
     @Override
    public  AppVersion queryAppVersionById(Long id){
         return appVersionMapper.queryAppVersionById(id);
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appVersion 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
      @Override
   public List<AppVersion> queryAllAppVersions(AppVersion appVersion,Integer from,Integer pageSize){
     return appVersionMapper.queryAllAppVersions(appVersion,(from-1)*pageSize, pageSize);
    }

    /**
     * 根据APPID查看APP版本信息
     *
     * @param appId APPid
     * @return 版本集合
     */
    @Override
    public List<AppVersion> getVersionsByAppId(Long appId) {
        return appVersionMapper.getVersionsByAppId(appId);
    }

    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appVersion 实例对象
     * @return 条数
     */
      @Override
   public int queryAllCount(AppVersion appVersion){
         return appVersionMapper.queryAllCount(appVersion);
    }

    /**
     * 新增数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
      @Override
  public  int insertAppVersion(AppVersion appVersion){
         return appVersionMapper.insertAppVersion(appVersion);
    }
    /**
     * 新增选择列
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
      @Override
   public int insertSelective(AppVersion appVersion){
         return appVersionMapper.insertSelective(appVersion);
    }
    
    /**
     * 修改数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
      @Override
   public int updateAppVersion(AppVersion appVersion){
         return appVersionMapper.updateAppVersion(appVersion);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
      @Override
   public int deleteAppVersionById(Long id){
         return appVersionMapper.deleteAppVersionById(id);
    }
    

    
}