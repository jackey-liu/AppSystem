package com.bdqn.jackey.service;
import com.bdqn.jackey.entity.AppCategory;
import java.util.List;
/**
 * @description：(AppCategory)表服务接口
 * @author：jackey
 * @create：
 */
public interface AppCategoryService {
 /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppCategory queryAppCategoryById(Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appCategory 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<AppCategory> queryAllAppCategorys(AppCategory appCategory,Integer from,Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appCategory 实例对象
     * @return 条数
     */
    int queryAllCount(AppCategory appCategory);

    /**
     * 新增数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int insertAppCategory(AppCategory appCategory);

    /**
     * 新增选择列
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int insertSelective(AppCategory appCategory);
    
    /**
     * 修改数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int updateAppCategory(AppCategory appCategory);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteAppCategoryById(Long id);

    
}