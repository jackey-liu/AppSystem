package com.bdqn.jackey.service.impl;

import com.bdqn.jackey.mapper.AppInfoMapper;
import com.bdqn.jackey.service.AppInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.bdqn.jackey.entity.AppInfo;
import java.util.List;

/**
 * @description：(AppInfo)表服务实现类
 * @author：jackey
 * @create：
 */
@Service("/appInfoService")
public class AppInfoServiceImpl implements AppInfoService {

    @Resource
    private AppInfoMapper appInfoMapper;

   /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
     @Override
    public  AppInfo queryAppInfoById(Long id){
         return appInfoMapper.queryAppInfoById(id);
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appInfo 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
      @Override
   public List<AppInfo> queryAllAppInfos(AppInfo appInfo,Integer from,Integer pageSize){
     return appInfoMapper.queryAllAppInfos(appInfo,(from-1)*pageSize, pageSize);
    }
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appInfo 实例对象
     * @return 条数
     */
      @Override
   public int queryAllCount(AppInfo appInfo){
         return appInfoMapper.queryAllCount(appInfo);
    }

    /**
     * 新增数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
      @Override
  public  int insertAppInfo(AppInfo appInfo){
         return appInfoMapper.insertAppInfo(appInfo);
    }
    /**
     * 新增选择列
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
      @Override
   public int insertSelective(AppInfo appInfo){
         return appInfoMapper.insertSelective(appInfo);
    }
    
    /**
     * 修改数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
      @Override
   public int updateAppInfo(AppInfo appInfo){
         return appInfoMapper.updateAppInfo(appInfo);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
      @Override
   public int deleteAppInfoById(Long id){
         return appInfoMapper.deleteAppInfoById(id);
    }
    

    
}