package com.bdqn.jackey.service;
import com.bdqn.jackey.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @description：(SysUser)表服务接口
 * @author：jackey
 * @create：
 */
public interface SysUserService {
   /**
    * 用户登录
    * @param user 用户信息
    * @return 登录结果
    */
   SysUser login(@Param("user") SysUser user);
 /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SysUser querySysUserById(Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysUser 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<SysUser> queryAllSysUsers(SysUser sysUser,Integer from,Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param sysUser 实例对象
     * @return 条数
     */
    int queryAllCount(SysUser sysUser);

    /**
     * 新增数据
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    int insertSysUser(SysUser sysUser);

    /**
     * 新增选择列
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    int insertSelective(SysUser sysUser);
    
    /**
     * 修改数据
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    int updateSysUser(SysUser sysUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteSysUserById(Long id);

    
}