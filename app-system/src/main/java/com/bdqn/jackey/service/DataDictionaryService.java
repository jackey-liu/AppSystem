package com.bdqn.jackey.service;
import com.bdqn.jackey.entity.DataDictionary;
import java.util.List;
/**
 * @description：(DataDictionary)表服务接口
 * @author：jackey
 * @create：
 */
public interface DataDictionaryService {
 /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    DataDictionary queryDataDictionaryById(Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param dataDictionary 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<DataDictionary> queryAllDataDictionarys(DataDictionary dataDictionary,Integer from,Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param dataDictionary 实例对象
     * @return 条数
     */
    int queryAllCount(DataDictionary dataDictionary);

    /**
     * 新增数据
     *
     * @param dataDictionary 实例对象
     * @return 影响行数
     */
    int insertDataDictionary(DataDictionary dataDictionary);

    /**
     * 新增选择列
     *
     * @param dataDictionary 实例对象
     * @return 影响行数
     */
    int insertSelective(DataDictionary dataDictionary);
    
    /**
     * 修改数据
     *
     * @param dataDictionary 实例对象
     * @return 影响行数
     */
    int updateDataDictionary(DataDictionary dataDictionary);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteDataDictionaryById(Long id);

    
}