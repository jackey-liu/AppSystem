package com.bdqn.jackey.service;
import com.bdqn.jackey.entity.SysRole;
import java.util.List;
/**
 * @description：(SysRole)表服务接口
 * @author：jackey
 * @create：
 */
public interface SysRoleService {
 /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SysRole querySysRoleById(Integer id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysRole 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<SysRole> queryAllSysRoles(SysRole sysRole,Integer from,Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param sysRole 实例对象
     * @return 条数
     */
    int queryAllCount(SysRole sysRole);

    /**
     * 新增数据
     *
     * @param sysRole 实例对象
     * @return 影响行数
     */
    int insertSysRole(SysRole sysRole);

    /**
     * 新增选择列
     *
     * @param sysRole 实例对象
     * @return 影响行数
     */
    int insertSelective(SysRole sysRole);
    
    /**
     * 修改数据
     *
     * @param sysRole 实例对象
     * @return 影响行数
     */
    int updateSysRole(SysRole sysRole);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteSysRoleById(Integer id);

    
}