package com.bdqn.jackey.service.impl;

import com.bdqn.jackey.mapper.SysUserMapper;
import com.bdqn.jackey.service.SysUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import com.bdqn.jackey.entity.SysUser;

import java.util.List;

/**
 * @description：(SysUser)表服务实现类
 * @author：jackey
 * @create：
 */
@Service("/sysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    /**
     * 用户登录
     *
     * @param user 用户信息
     * @return 登录结果
     */
    @Override
    public SysUser login(@Param("user") SysUser user) {
        return sysUserMapper.login(user);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public SysUser querySysUserById(Long id) {
        return sysUserMapper.querySysUserById(id);
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysUser  实例对象
     * @param from     分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    @Override
    public List<SysUser> queryAllSysUsers(SysUser sysUser, Integer from, Integer pageSize) {
        return sysUserMapper.queryAllSysUsers(sysUser, (from-1)*pageSize, pageSize);
    }

    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param sysUser 实例对象
     * @return 条数
     */
    @Override
    public int queryAllCount(SysUser sysUser) {
        return sysUserMapper.queryAllCount(sysUser);
    }

    /**
     * 新增数据
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    @Override
    public int insertSysUser(SysUser sysUser) {
        return sysUserMapper.insertSysUser(sysUser);
    }

    /**
     * 新增选择列
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    @Override
    public int insertSelective(SysUser sysUser) {
        return sysUserMapper.insertSelective(sysUser);
    }

    /**
     * 修改数据
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    @Override
    public int updateSysUser(SysUser sysUser) {
        return sysUserMapper.updateSysUser(sysUser);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    @Override
    public int deleteSysUserById(Long id) {
        return sysUserMapper.deleteSysUserById(id);
    }


}