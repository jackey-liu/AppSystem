package com.bdqn.jackey.service.impl;

import com.bdqn.jackey.mapper.DataDictionaryMapper;
import com.bdqn.jackey.service.DataDictionaryService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.bdqn.jackey.entity.DataDictionary;
import java.util.List;

/**
 * @description：(DataDictionary)表服务实现类
 * @author：jackey
 * @create：
 */
@Service("/dataDictionaryService")
public class DataDictionaryServiceImpl implements DataDictionaryService {

    @Resource
    private DataDictionaryMapper dataDictionaryMapper;

   /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
     @Override
    public  DataDictionary queryDataDictionaryById(Long id){
         return dataDictionaryMapper.queryDataDictionaryById(id);
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @param dataDictionary 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
      @Override
   public List<DataDictionary> queryAllDataDictionarys(DataDictionary dataDictionary,Integer from,Integer pageSize){
     return dataDictionaryMapper.queryAllDataDictionaries(dataDictionary,(from-1)*pageSize, pageSize);
    }
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param dataDictionary 实例对象
     * @return 条数
     */
      @Override
   public int queryAllCount(DataDictionary dataDictionary){
         return dataDictionaryMapper.queryAllCount(dataDictionary);
    }

    /**
     * 新增数据
     *
     * @param dataDictionary 实例对象
     * @return 影响行数
     */
      @Override
  public  int insertDataDictionary(DataDictionary dataDictionary){
         return dataDictionaryMapper.insertDataDictionary(dataDictionary);
    }
    /**
     * 新增选择列
     *
     * @param dataDictionary 实例对象
     * @return 影响行数
     */
      @Override
   public int insertSelective(DataDictionary dataDictionary){
         return dataDictionaryMapper.insertSelective(dataDictionary);
    }
    
    /**
     * 修改数据
     *
     * @param dataDictionary 实例对象
     * @return 影响行数
     */
      @Override
   public int updateDataDictionary(DataDictionary dataDictionary){
         return dataDictionaryMapper.updateDataDictionary(dataDictionary);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
      @Override
   public int deleteDataDictionaryById(Long id){
         return dataDictionaryMapper.deleteDataDictionaryById(id);
    }
    

    
}