package com.bdqn.jackey.service.impl;

import com.bdqn.jackey.mapper.SysRoleMapper;
import com.bdqn.jackey.service.SysRoleService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.bdqn.jackey.entity.SysRole;
import java.util.List;

/**
 * @description：(SysRole)表服务实现类
 * @author：jackey
 * @create：
 */
@Service("/sysRoleService")
public class SysRoleServiceImpl implements SysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;

   /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
     @Override
    public  SysRole querySysRoleById(Integer id){
         return sysRoleMapper.querySysRoleById(id);
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysRole 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
      @Override
   public List<SysRole> queryAllSysRoles(SysRole sysRole,Integer from,Integer pageSize){
     return sysRoleMapper.queryAllSysRoles(sysRole,(from-1)*pageSize, pageSize);
    }
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param sysRole 实例对象
     * @return 条数
     */
      @Override
   public int queryAllCount(SysRole sysRole){
         return sysRoleMapper.queryAllCount(sysRole);
    }

    /**
     * 新增数据
     *
     * @param sysRole 实例对象
     * @return 影响行数
     */
      @Override
  public  int insertSysRole(SysRole sysRole){
         return sysRoleMapper.insertSysRole(sysRole);
    }
    /**
     * 新增选择列
     *
     * @param sysRole 实例对象
     * @return 影响行数
     */
      @Override
   public int insertSelective(SysRole sysRole){
         return sysRoleMapper.insertSelective(sysRole);
    }
    
    /**
     * 修改数据
     *
     * @param sysRole 实例对象
     * @return 影响行数
     */
      @Override
   public int updateSysRole(SysRole sysRole){
         return sysRoleMapper.updateSysRole(sysRole);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
      @Override
   public int deleteSysRoleById(Integer id){
         return sysRoleMapper.deleteSysRoleById(id);
    }
    

    
}