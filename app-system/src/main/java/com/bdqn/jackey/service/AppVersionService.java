package com.bdqn.jackey.service;
import com.bdqn.jackey.entity.AppVersion;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * @description：(AppVersion)表服务接口
 * @author：jackey
 * @create：
 */
public interface AppVersionService {
 /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppVersion queryAppVersionById(Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appVersion 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<AppVersion> queryAllAppVersions(AppVersion appVersion,Integer from,Integer pageSize);

   /**
    * 根据APPID查看APP版本信息
    * @param appId APPid
    * @return 版本集合
    */
   List<AppVersion> getVersionsByAppId(Long appId);
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appVersion 实例对象
     * @return 条数
     */
    int queryAllCount(AppVersion appVersion);

    /**
     * 新增数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int insertAppVersion(AppVersion appVersion);

    /**
     * 新增选择列
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int insertSelective(AppVersion appVersion);
    
    /**
     * 修改数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int updateAppVersion(AppVersion appVersion);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteAppVersionById(Long id);

    
}