package com.bdqn.jackey.service;
import com.bdqn.jackey.entity.AppInfo;
import java.util.List;
/**
 * @description：(AppInfo)表服务接口
 * @author：jackey
 * @create：
 */
public interface AppInfoService {
 /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppInfo queryAppInfoById(Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appInfo 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<AppInfo> queryAllAppInfos(AppInfo appInfo,Integer from,Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appInfo 实例对象
     * @return 条数
     */
    int queryAllCount(AppInfo appInfo);

    /**
     * 新增数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int insertAppInfo(AppInfo appInfo);

    /**
     * 新增选择列
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int insertSelective(AppInfo appInfo);
    
    /**
     * 修改数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int updateAppInfo(AppInfo appInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteAppInfoById(Long id);

    
}