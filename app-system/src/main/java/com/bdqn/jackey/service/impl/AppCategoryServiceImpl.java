package com.bdqn.jackey.service.impl;

import com.bdqn.jackey.mapper.AppCategoryMapper;
import com.bdqn.jackey.service.AppCategoryService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.bdqn.jackey.entity.AppCategory;
import java.util.List;

/**
 * @description：(AppCategory)表服务实现类
 * @author：jackey
 * @create：
 */
@Service("/appCategoryService")
public class AppCategoryServiceImpl implements AppCategoryService {

    @Resource
    private AppCategoryMapper appCategoryMapper;

   /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
     @Override
    public  AppCategory queryAppCategoryById(Long id){
         return appCategoryMapper.queryAppCategoryById(id);
    }

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appCategory 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
      @Override
   public List<AppCategory> queryAllAppCategorys(AppCategory appCategory,Integer from,Integer pageSize){
     return appCategoryMapper.queryAllAppCategorys(appCategory,(from-1)*pageSize, pageSize);
    }
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appCategory 实例对象
     * @return 条数
     */
      @Override
   public int queryAllCount(AppCategory appCategory){
         return appCategoryMapper.queryAllCount(appCategory);
    }

    /**
     * 新增数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
      @Override
  public  int insertAppCategory(AppCategory appCategory){
         return appCategoryMapper.insertAppCategory(appCategory);
    }
    /**
     * 新增选择列
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
      @Override
   public int insertSelective(AppCategory appCategory){
         return appCategoryMapper.insertSelective(appCategory);
    }
    
    /**
     * 修改数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
      @Override
   public int updateAppCategory(AppCategory appCategory){
         return appCategoryMapper.updateAppCategory(appCategory);
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
      @Override
   public int deleteAppCategoryById(Long id){
         return appCategoryMapper.deleteAppCategoryById(id);
    }
    

    
}