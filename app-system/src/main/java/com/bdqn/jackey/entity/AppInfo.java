package com.bdqn.jackey.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;

/**
 * @description：(AppInfo)实体类
 * @author：jackey
 * @create：
 */
public class AppInfo implements Serializable {

    private static final long serialVersionUID = 605050022314430028L;
        /**
     * 主键id
     */
    private Long id;
        /**
     * 软件名称
     */
    private String softwareName;
        /**
     * APK名称（唯一）
     */
    private String apkName;
        /**
     * 支持ROM
     */
    private String supportRom;
        /**
     * 界面语言
     */
    private String interfaceLanguage;
        /**
     * 软件大小（单位：M）
     */
    private Double softwareSize;
        /**
     * 更新日期
     */
    private Date updateTime;
        /**
     * 开发者id（来源于：dev_user表的开发者id）
     */
    private Long devId;
        /**
     * 应用简介
     */
    private String appinfo;
        /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    private Long status;
        /**
     * 上架时间
     */
    private Date onsaledate;
        /**
     * 下架时间
     */
    private Date offsaledate;
        /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    private Long flatformId;
        /**
     * 所属一级分类（来源于：data_dictionary）
     */
    private Long categorylevel1;
        /**
     * 所属二级分类（来源于：data_dictionary）
     */
    private Long categorylevel2;
        /**
     * 所属三级分类（来源于：data_dictionary）
     */
    private Long categorylevel3;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    private String flatformName;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    private String categorylevel1Name;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    private String categorylevel2Name;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    private String categorylevel3Name;

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    /**
     * 删除状态 0-已删除 1-正常
     */
    private String delFlag;

    public String getFlatformName() {
        return flatformName;
    }

    public void setFlatformName(String flatformName) {
        this.flatformName = flatformName;
    }

    public String getCategorylevel1Name() {
        return categorylevel1Name;
    }

    public void setCategorylevel1Name(String categorylevel1Name) {
        this.categorylevel1Name = categorylevel1Name;
    }

    public String getCategorylevel2Name() {
        return categorylevel2Name;
    }

    public void setCategorylevel2Name(String categorylevel2Name) {
        this.categorylevel2Name = categorylevel2Name;
    }

    public String getCategorylevel3Name() {
        return categorylevel3Name;
    }

    public void setCategorylevel3Name(String categorylevel3Name) {
        this.categorylevel3Name = categorylevel3Name;
    }

    /**
     * 下载量（单位：次）
     */
    private Long downloads;
        /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    private String createBy;
        /**
     * 创建时间
     */
    private Date createTime;
        /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    private String modifyBy;
        /**
     * 最新更新时间
     */
    private Date modifyTime;
        /**
     * LOGO图片url路径
     */
    private String logoPath;
        /**
     * LOGO图片的服务器存储路径
     */
    private String logolocpath;
        /**
     * 最新的版本id
     */
    private Long versionid;

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    /**
     * 最新版本号
     */
    private String versionNo;
        
    public AppInfo() {
    }
    
    public AppInfo(Long id , String softwareName , String apkName , String supportRom , String interfaceLanguage , Double softwareSize , Date updateTime , Long devId , String appinfo , Long status , Date onsaledate , Date offsaledate , Long flatformId , Long categorylevel1 , Long categorylevel2 , Long categorylevel3 , Long downloads , String createBy , Date createTime , String modifyBy , Date modifyTime , String logoPath , String logolocpath , Long versionid ) {
          this.id = id;  this.softwareName = softwareName;  this.apkName = apkName;  this.supportRom = supportRom;  this.interfaceLanguage = interfaceLanguage;  this.softwareSize = softwareSize;  this.updateTime = updateTime;  this.devId = devId;  this.appinfo = appinfo;  this.status = status;  this.onsaledate = onsaledate;  this.offsaledate = offsaledate;  this.flatformId = flatformId;  this.categorylevel1 = categorylevel1;  this.categorylevel2 = categorylevel2;  this.categorylevel3 = categorylevel3;  this.downloads = downloads;  this.createBy = createBy;  this.createTime = createTime;  this.modifyBy = modifyBy;  this.modifyTime = modifyTime;  this.logoPath = logoPath;  this.logolocpath = logolocpath;  this.versionid = versionid;        
    }
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
         this.id = id;} 
        
    public String getSoftwareName() {
        return softwareName;
    }

    public void setSoftwareName(String softwareName) {
        this.softwareName = softwareName;
    } 
        
    public String getApkName() {
        return apkName;
    }

    public void setApkName(String apkName) {
        this.apkName = apkName;
    } 
        
    public String getSupportRom() {
        return supportRom;
    }

    public void setSupportRom(String supportRom) {
        this.supportRom = supportRom;
    } 
        
    public String getInterfaceLanguage() {
        return interfaceLanguage;
    }

    public void setInterfaceLanguage(String interfaceLanguage) {
        this.interfaceLanguage = interfaceLanguage;
    } 
        
    public Double getSoftwareSize() {
        return softwareSize;
    }

    public void setSoftwareSize(Double softwareSize) {
         this.softwareSize = softwareSize;} 
        
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
         this.updateTime = updateTime;} 
        
    public Long getDevId() {
        return devId;
    }

    public void setDevId(Long devId) {
         this.devId = devId;} 
        
    public String getAppinfo() {
        return appinfo;
    }

    public void setAppinfo(String appinfo) {
        this.appinfo = appinfo;
    } 
        
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
         this.status = status;} 
        
    public Date getOnsaledate() {
        return onsaledate;
    }

    public void setOnsaledate(Date onsaledate) {
         this.onsaledate = onsaledate;} 
        
    public Date getOffsaledate() {
        return offsaledate;
    }

    public void setOffsaledate(Date offsaledate) {
         this.offsaledate = offsaledate;} 
        
    public Long getFlatformId() {
        return flatformId;
    }

    public void setFlatformId(Long flatformId) {
         this.flatformId = flatformId;} 
        
    public Long getCategorylevel1() {
        return categorylevel1;
    }

    public void setCategorylevel1(Long categorylevel1) {
         this.categorylevel1 = categorylevel1;} 
        
    public Long getCategorylevel2() {
        return categorylevel2;
    }

    public void setCategorylevel2(Long categorylevel2) {
         this.categorylevel2 = categorylevel2;} 
        
    public Long getCategorylevel3() {
        return categorylevel3;
    }

    public void setCategorylevel3(Long categorylevel3) {
         this.categorylevel3 = categorylevel3;} 
        
    public Long getDownloads() {
        return downloads;
    }

    public void setDownloads(Long downloads) {
         this.downloads = downloads;} 
        
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
         this.createBy = createBy;} 
        
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
         this.createTime = createTime;} 
        
    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
         this.modifyBy = modifyBy;} 
        
    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
         this.modifyTime = modifyTime;} 
        
    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath== null ? null : logoPath.trim();
    } 
        
    public String getLogolocpath() {
        return logolocpath;
    }

    public void setLogolocpath(String logolocpath) {
        this.logolocpath = logolocpath== null ? null : logolocpath.trim();
    } 
        
    public Long getVersionid() {
        return versionid;
    }

    public void setVersionid(Long versionid) {
         this.versionid = versionid;} 

    @Override
    public String toString() { 
        StringBuilder str = new StringBuilder();
        str.append("AppInfo{");       
        str.append("id=").append(this.id).append(", ");
        str.append("softwareName=").append(this.softwareName).append(", ");
        str.append("apkName=").append(this.apkName).append(", ");
        str.append("supportRom=").append(this.supportRom).append(", ");
        str.append("interfaceLanguage=").append(this.interfaceLanguage).append(", ");
        str.append("softwareSize=").append(this.softwareSize).append(", ");
        str.append("updateTime=").append(this.updateTime).append(", ");
        str.append("devId=").append(this.devId).append(", ");
        str.append("appinfo=").append(this.appinfo).append(", ");
        str.append("status=").append(this.status).append(", ");
        str.append("onsaledate=").append(this.onsaledate).append(", ");
        str.append("offsaledate=").append(this.offsaledate).append(", ");
        str.append("flatformId=").append(this.flatformId).append(", ");
        str.append("categorylevel1=").append(this.categorylevel1).append(", ");
        str.append("categorylevel2=").append(this.categorylevel2).append(", ");
        str.append("categorylevel3=").append(this.categorylevel3).append(", ");
        str.append("downloads=").append(this.downloads).append(", ");
        str.append("createBy=").append(this.createBy).append(", ");
        str.append("createTime=").append(this.createTime).append(", ");
        str.append("modifyBy=").append(this.modifyBy).append(", ");
        str.append("modifyTime=").append(this.modifyTime).append(", ");
        str.append("logoPath=").append(this.logoPath).append(", ");
        str.append("logolocpath=").append(this.logolocpath).append(", ");
        str.append("versionid=").append(this.versionid);
        str.append('}');       
        return str.toString();
    }

}