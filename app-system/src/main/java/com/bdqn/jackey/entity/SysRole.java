package com.bdqn.jackey.entity;

import java.io.Serializable;

/**
 * @description：(SysRole)实体类
 * @author：jackey
 * @create：
 */
public class SysRole implements Serializable {

    private static final long serialVersionUID = -39780892851911206L;
        /**
     * role id
     */
    private Integer id;
        /**
     * 角色名称
     */
    private String roleName;
        
    public SysRole() {
    }
    
    public SysRole(Integer id , String roleName ) {
          this.id = id;  this.roleName = roleName;        
    }
        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
         this.id = id;} 
        
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    } 

    @Override
    public String toString() { 
        StringBuilder str = new StringBuilder();
        str.append("SysRole{");       
        str.append("id=").append(this.id).append(", ");
        str.append("roleName=").append(this.roleName);
        str.append('}');       
        return str.toString();
    }

}