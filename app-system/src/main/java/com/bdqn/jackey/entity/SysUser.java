package com.bdqn.jackey.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * @description：(SysUser)实体类
 * @author：jackey
 * @create：
 */
public class SysUser implements Serializable {

    private static final long serialVersionUID = -64622168805651833L;
        /**
     * 主键id
     */
    private Long id;
        /**
     * 用户编码
     */
    private String userCode;
        /**
     * 用户名称
     */
    private String userName;
        /**
     * 用户密码
     */
    private String userPassword;
        /**
     * 用户角色id，来自于角色表
     */
    private Long userRole;
    /**
     * 用户角色名称
     */
    private String userRoleName;

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    /**
     * 电话
     */
    private String phone;
        /**
     * 邮箱
     */
    private String email;
        /**
     * 开发者简介
     */
    private String userIntro;
        /**
     * 用户状态，0代表已停用，1代表正常
     */
    private Long status;
        /**
     * 逻辑删除状态，0代表已删除，1代表正常
     */
    private Long delFlag;
        /**
     * 创建者（来源于sys_user用户表的username）
     */
    private String createBy;
        /**
     * 创建时间
     */
    private Date createTime;
        /**
     * 更新者（来源于sys_user用户表的username）
     */
    private String modifyBy;
        /**
     * 最新更新时间
     */
    private Date modifyTime;
        
    public SysUser() {
    }
    
    public SysUser(Long id, String userCode, String userName, String userPassword, Long userRole, String userRoleName, String phone, String email, String userIntro, Long status, Long delFlag, String createBy, Date createTime, String modifyBy, Date modifyTime) {
          this.id = id;  this.userCode = userCode;  this.userName = userName;  this.userPassword = userPassword;  this.userRole = userRole;
        this.userRoleName = userRoleName;
        this.phone = phone;  this.email = email;  this.userIntro = userIntro;  this.status = status;  this.delFlag = delFlag;  this.createBy = createBy;  this.createTime = createTime;  this.modifyBy = modifyBy;  this.modifyTime = modifyTime;
    }
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
         this.id = id;} 
        
    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    } 
        
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    } 
        
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    } 
        
    public Long getUserRole() {
        return userRole;
    }

    public void setUserRole(Long userRole) {
         this.userRole = userRole;} 
        
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    } 
        
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    } 
        
    public String getUserIntro() {
        return userIntro;
    }

    public void setUserIntro(String userIntro) {
        this.userIntro = userIntro;
    } 
        
    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
         this.status = status;} 
        
    public Long getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Long delFlag) {
         this.delFlag = delFlag;} 
        
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    } 
        
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
         this.createTime = createTime;} 
        
    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    } 
        
    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
         this.modifyTime = modifyTime;} 

    @Override
    public String toString() { 
        StringBuilder str = new StringBuilder();
        str.append("SysUser{");       
        str.append("id=").append(this.id).append(", ");
        str.append("userCode=").append(this.userCode).append(", ");
        str.append("userName=").append(this.userName).append(", ");
        str.append("userPassword=").append(this.userPassword).append(", ");
        str.append("userRole=").append(this.userRole).append(", ");
        str.append("phone=").append(this.phone).append(", ");
        str.append("email=").append(this.email).append(", ");
        str.append("userIntro=").append(this.userIntro).append(", ");
        str.append("status=").append(this.status).append(", ");
        str.append("delFlag=").append(this.delFlag).append(", ");
        str.append("createBy=").append(this.createBy).append(", ");
        str.append("createTime=").append(this.createTime).append(", ");
        str.append("modifyBy=").append(this.modifyBy).append(", ");
        str.append("modifyTime=").append(this.modifyTime);
        str.append('}');       
        return str.toString();
    }

}