package com.bdqn.jackey.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * @description：(DataDictionary)实体类
 * @author：jackey
 * @create：
 */
public class DataDictionary implements Serializable {

    private static final long serialVersionUID = 301232410804026593L;
        /**
     * 主键ID
     */
    private Long id;
        /**
     * 类型编码
     */
    private String typeCode;
        /**
     * 类型名称
     */
    private String typeName;
        /**
     * 类型值ID
     */
    private Long valueId;
        /**
     * 类型值Name
     */
    private String valueName;
        /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private String createBy;
        /**
     * 创建时间
     */
    private Date createTime;
        /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private String modifyBy;
        /**
     * 最新更新时间
     */
    private Date modifyTime;
        
    public DataDictionary() {
    }
    
    public DataDictionary(Long id , String typeCode , String typeName , Long valueId , String valueName , String createBy , Date createTime , String modifyBy , Date modifyTime ) {
          this.id = id;  this.typeCode = typeCode;  this.typeName = typeName;  this.valueId = valueId;  this.valueName = valueName;  this.createBy = createBy;  this.createTime = createTime;  this.modifyBy = modifyBy;  this.modifyTime = modifyTime;        
    }
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
         this.id = id;} 
        
    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    } 
        
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    } 
        
    public Long getValueId() {
        return valueId;
    }

    public void setValueId(Long valueId) {
         this.valueId = valueId;} 
        
    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    } 
        
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
         this.createBy = createBy;} 
        
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
         this.createTime = createTime;} 
        
    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
         this.modifyBy = modifyBy;} 
        
    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
         this.modifyTime = modifyTime;} 

    @Override
    public String toString() { 
        StringBuilder str = new StringBuilder();
        str.append("DataDictionary{");       
        str.append("id=").append(this.id).append(", ");
        str.append("typeCode=").append(this.typeCode).append(", ");
        str.append("typeName=").append(this.typeName).append(", ");
        str.append("valueId=").append(this.valueId).append(", ");
        str.append("valueName=").append(this.valueName).append(", ");
        str.append("createBy=").append(this.createBy).append(", ");
        str.append("createTime=").append(this.createTime).append(", ");
        str.append("modifyBy=").append(this.modifyBy).append(", ");
        str.append("modifyTime=").append(this.modifyTime);
        str.append('}');       
        return str.toString();
    }

}