package com.bdqn.jackey.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * @description：(AppVersion)实体类
 * @author：jackey
 * @create：
 */
public class AppVersion implements Serializable {

    private static final long serialVersionUID = -61545118582726848L;
        /**
     * 主键id
     */
    private Long id;
        /**
     * appId（来源于：app_info表的主键id）
     */
    private Long appId;
        /**
     * 版本号
     */
    private String versionNo;
        /**
     * 版本介绍
     */
    private String versionInfo;
        /**
     * 发布状态（来源于：data_dictionary，1 不发布 2 已发布 3 预发布）
     */
    private Long publishStatus;
        /**
     * 下载链接
     */
    private String downloadLink;
        /**
     * 版本大小（单位：M）
     */
    private Double versionSize;
        /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    private String createBy;
        /**
     * 创建时间
     */
    private Date createTime;
        /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    private String modifyBy;

    public AppVersion(Long appId) {
        this.appId = appId;
    }

    /**
     * 最新更新时间
     */
    private Date modifyTime;
        /**
     * apk文件的服务器存储路径
     */
    private String apkLocPath;
        /**
     * 上传的apk文件名称
     */
    private String apkFilename;
    /**
     * 发布状态字典名称
     */
    private String publishStatusName;

    public String getPublishStatusName() {
        return publishStatusName;
    }

    public void setPublishStatusName(String publishStatusName) {
        this.publishStatusName = publishStatusName;
    }

    public AppVersion() {
    }

    public AppVersion(Long id , Long appId , String versionNo , String versionInfo , Long publishStatus , String downloadLink , Double versionSize , String createBy , Date createTime , String modifyBy , Date modifyTime , String apkLocPath , String apkFilename ) {
          this.id = id;  this.appId = appId;  this.versionNo = versionNo;  this.versionInfo = versionInfo;  this.publishStatus = publishStatus;  this.downloadLink = downloadLink;  this.versionSize = versionSize;  this.createBy = createBy;  this.createTime = createTime;  this.modifyBy = modifyBy;  this.modifyTime = modifyTime;  this.apkLocPath = apkLocPath;  this.apkFilename = apkFilename;        
    }
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
         this.id = id;} 
        
    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
         this.appId = appId;} 
        
    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    } 
        
    public String getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(String versionInfo) {
        this.versionInfo = versionInfo;
    } 
        
    public Long getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(Long publishStatus) {
         this.publishStatus = publishStatus;} 
        
    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    } 
        
    public Double getVersionSize() {
        return versionSize;
    }

    public void setVersionSize(Double versionSize) {
         this.versionSize = versionSize;} 
        
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
         this.createBy = createBy;} 
        
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
         this.createTime = createTime;} 
        
    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
         this.modifyBy = modifyBy;} 
        
    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
         this.modifyTime = modifyTime;} 
        
    public String getApkLocPath() {
        return apkLocPath;
    }

    public void setApkLocPath(String apkLocPath) {
        this.apkLocPath = apkLocPath;
    } 
        
    public String getApkFilename() {
        return apkFilename;
    }

    public void setApkFilename(String apkFilename) {
        this.apkFilename = apkFilename;
    } 

    @Override
    public String toString() { 
        StringBuilder str = new StringBuilder();
        str.append("AppVersion{");       
        str.append("id=").append(this.id).append(", ");
        str.append("appId=").append(this.appId).append(", ");
        str.append("versionNo=").append(this.versionNo).append(", ");
        str.append("versionInfo=").append(this.versionInfo).append(", ");
        str.append("publishStatus=").append(this.publishStatus).append(", ");
        str.append("downloadLink=").append(this.downloadLink).append(", ");
        str.append("versionSize=").append(this.versionSize).append(", ");
        str.append("createBy=").append(this.createBy).append(", ");
        str.append("createTime=").append(this.createTime).append(", ");
        str.append("modifyBy=").append(this.modifyBy).append(", ");
        str.append("modifyTime=").append(this.modifyTime).append(", ");
        str.append("apkLocPath=").append(this.apkLocPath).append(", ");
        str.append("apkFilename=").append(this.apkFilename);
        str.append('}');       
        return str.toString();
    }

}