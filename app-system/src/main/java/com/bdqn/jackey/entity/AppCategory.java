package com.bdqn.jackey.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * @description：(AppCategory)实体类
 * @author：jackey
 * @create：
 */
public class AppCategory implements Serializable {

    private static final long serialVersionUID = 775578335560701867L;
        /**
     * 主键ID
     */
    private Long id;
        /**
     * 分类编码
     */
    private String categoryCode;
        /**
     * 分类名称
     */
    private String categoryName;
        /**
     * 父级节点id
     */
    private Long parentid;
        /**
     * 创建者（来源于sys_user用户表的用户id）
     */
    private String createBy;
        /**
     * 创建时间
     */
    private Date createTime;
        /**
     * 更新者（来源于sys_user用户表的用户id）
     */
    private String modifyBy;
        /**
     * 最新更新时间
     */
    private Date modifyTime;
        
    public AppCategory() {
    }
    
    public AppCategory(Long id , String categoryCode , String categoryName , Long parentid , String createBy , Date createTime , String modifyBy , Date modifyTime ) {
          this.id = id;  this.categoryCode = categoryCode;  this.categoryName = categoryName;  this.parentid = parentid;  this.createBy = createBy;  this.createTime = createTime;  this.modifyBy = modifyBy;  this.modifyTime = modifyTime;        
    }
        
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
         this.id = id;} 
        
    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    } 
        
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    } 
        
    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
         this.parentid = parentid;} 
        
    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
         this.createBy = createBy;} 
        
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
         this.createTime = createTime;} 
        
    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
         this.modifyBy = modifyBy;} 
        
    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
         this.modifyTime = modifyTime;} 

    @Override
    public String toString() { 
        StringBuilder str = new StringBuilder();
        str.append("AppCategory{");       
        str.append("id=").append(this.id).append(", ");
        str.append("categoryCode=").append(this.categoryCode).append(", ");
        str.append("categoryName=").append(this.categoryName).append(", ");
        str.append("parentid=").append(this.parentid).append(", ");
        str.append("createBy=").append(this.createBy).append(", ");
        str.append("createTime=").append(this.createTime).append(", ");
        str.append("modifyBy=").append(this.modifyBy).append(", ");
        str.append("modifyTime=").append(this.modifyTime);
        str.append('}');       
        return str.toString();
    }

}