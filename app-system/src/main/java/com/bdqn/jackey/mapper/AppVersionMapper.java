package com.bdqn.jackey.mapper;

import com.bdqn.jackey.entity.AppVersion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @description：(AppVersion)表数据库访问层
 * @author：jackey
 * @create：
 */
@Mapper
public interface AppVersionMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppVersion queryAppVersionById(@Param("id") Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appVersion 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<AppVersion> queryAllAppVersions(@Param("appVersion") AppVersion appVersion,@Param("from")Integer from,@Param("pageSize")Integer pageSize);

    /**
     * 根据APPID查看APP版本信息
     * @param appId APPid
     * @return 版本集合
     */
    List<AppVersion> getVersionsByAppId(@Param("appId") Long appId);
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appVersion 实例对象
     * @return 条数
     */
    int queryAllCount(@Param("appVersion")AppVersion appVersion);

    /**
     * 新增数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int insertAppVersion(@Param("appVersion") AppVersion appVersion);

    /**
     * 新增选择列
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int insertSelective(@Param("appVersion") AppVersion appVersion);
    
    /**
     * 修改数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int updateAppVersion(@Param("appVersion")AppVersion appVersion);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteAppVersionById(@Param("id") Long id);
    
    /**
     * 批量新增
     * @param recordList
     * @return 影响行数
     */
    int batchInsert(@Param("recordList") List<AppVersion> recordList);
    
    /**
     * 批量修改
     * @param recordList 
     * @return 影响行数
     */
    int batchUpdate(@Param("recordList") List<AppVersion> recordList);
    
    /**
     * 根据主键批量删除
     * @param ids 主键s
     * @return 影响行数
     */
    int batchDelete(@Param("ids") String[] ids);
    
}