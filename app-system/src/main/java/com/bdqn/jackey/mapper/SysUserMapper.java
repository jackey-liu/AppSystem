package com.bdqn.jackey.mapper;

import com.bdqn.jackey.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @description：(SysUser)表数据库访问层
 * @author：jackey
 * @create：
 */
@Mapper
public interface SysUserMapper {
    /**
     * 用户登录
     * @param sysUser 用户信息
     * @return 登录结果
     */
    SysUser login(@Param("sysUser") SysUser sysUser);
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SysUser querySysUserById(@Param("id") Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param sysUser 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<SysUser> queryAllSysUsers(@Param("sysUser") SysUser sysUser,@Param("from")Integer from,@Param("pageSize")Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param sysUser 实例对象
     * @return 条数
     */
    int queryAllCount(@Param("sysUser")SysUser sysUser);

    /**
     * 新增数据
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    int insertSysUser(@Param("sysUser") SysUser sysUser);

    /**
     * 新增选择列
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    int insertSelective(@Param("sysUser") SysUser sysUser);
    
    /**
     * 修改数据
     *
     * @param sysUser 实例对象
     * @return 影响行数
     */
    int updateSysUser(@Param("sysUser")SysUser sysUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteSysUserById(@Param("id") Long id);
    
    /**
     * 批量新增
     * @param recordList
     * @return 影响行数
     */
    int batchInsert(@Param("recordList") List<SysUser> recordList);
    
    /**
     * 批量修改
     * @param recordList 
     * @return 影响行数
     */
    int batchUpdate(@Param("recordList") List<SysUser> recordList);
    
    /**
     * 根据主键批量删除
     * @param ids 主键s
     * @return 影响行数
     */
    int batchDelete(@Param("ids") String[] ids);
    
}