package com.bdqn.jackey.mapper;

import com.bdqn.jackey.entity.AppCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @description：(AppCategory)表数据库访问层
 * @author：jackey
 * @create：
 */
@Mapper
public interface AppCategoryMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppCategory queryAppCategoryById(@Param("id") Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appCategory 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<AppCategory> queryAllAppCategorys(@Param("appCategory") AppCategory appCategory,@Param("from")Integer from,@Param("pageSize")Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appCategory 实例对象
     * @return 条数
     */
    int queryAllCount(@Param("appCategory")AppCategory appCategory);

    /**
     * 新增数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int insertAppCategory(@Param("appCategory") AppCategory appCategory);

    /**
     * 新增选择列
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int insertSelective(@Param("appCategory") AppCategory appCategory);
    
    /**
     * 修改数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int updateAppCategory(@Param("appCategory")AppCategory appCategory);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteAppCategoryById(@Param("id") Long id);
    
    /**
     * 批量新增
     * @param recordList
     * @return 影响行数
     */
    int batchInsert(@Param("recordList") List<AppCategory> recordList);
    
    /**
     * 批量修改
     * @param recordList 
     * @return 影响行数
     */
    int batchUpdate(@Param("recordList") List<AppCategory> recordList);
    
    /**
     * 根据主键批量删除
     * @param ids 主键s
     * @return 影响行数
     */
    int batchDelete(@Param("ids") String[] ids);
    
}