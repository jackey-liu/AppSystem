package com.bdqn.jackey.mapper;

import com.bdqn.jackey.entity.AppInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @description：(AppInfo)表数据库访问层
 * @author：jackey
 * @create：
 */
@Mapper
public interface AppInfoMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppInfo queryAppInfoById(@Param("id") Long id);

    /**
     * 通过实体作为筛选条件查询
     *
     * @param appInfo 实例对象
     * @param from 分页起始位置
     * @param pageSize 页面大小
     * @return 对象列表
     */
    List<AppInfo> queryAllAppInfos(@Param("appInfo") AppInfo appInfo,@Param("from")Integer from,@Param("pageSize")Integer pageSize);
    
    /**
     * 通过实体作为筛选条件查询条数
     *
     * @param appInfo 实例对象
     * @return 条数
     */
    int queryAllCount(@Param("appInfo")AppInfo appInfo);

    /**
     * 新增数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int insertAppInfo(@Param("appInfo") AppInfo appInfo);

    /**
     * 新增选择列
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int insertSelective(@Param("appInfo") AppInfo appInfo);
    
    /**
     * 修改数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int updateAppInfo(@Param("appInfo")AppInfo appInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteAppInfoById(@Param("id") Long id);
    
    /**
     * 批量新增
     * @param recordList
     * @return 影响行数
     */
    int batchInsert(@Param("recordList") List<AppInfo> recordList);
    
    /**
     * 批量修改
     * @param recordList 
     * @return 影响行数
     */
    int batchUpdate(@Param("recordList") List<AppInfo> recordList);
    
    /**
     * 根据主键批量删除
     * @param ids 主键s
     * @return 影响行数
     */
    int batchDelete(@Param("ids") String[] ids);
    
}