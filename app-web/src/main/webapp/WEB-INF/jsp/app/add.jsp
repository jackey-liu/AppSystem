<%--
  Created by IntelliJ IDEA.
  User: 贾松泊
  Date: 2020/10/17
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html; UTF-8" isELIgnored="false" %>
<%@ include file="../common/contextPath.jsp" %>
<html>
<title>新增APP基础信息</title>
<link rel="stylesheet" href="${ctx}/statics/layui/css/layui.css">
<body>
<form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label">软件名称*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入软件名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">APK名称*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入APK名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">支持ROM*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入支持的ROM" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">界面语言*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入软件支持的界面语言" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">软件大小*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入软件大小，单位mb" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">下载次数*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入下载次数" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">所属平台*</label>
        <div class="layui-input-block">
            <select name="city" lay-verify="required">
            </select>
        </div>

    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">一级分类*</label>
        <div class="layui-input-block">
            <select name="city" lay-verify="required">
            </select>
        </div>

    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">二级分类*</label>
        <div class="layui-input-block">
            <select name="city" lay-verify="required">
            </select>
        </div>

    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">三级分类*</label>
        <div class="layui-input-block">
            <select name="city" lay-verify="required">
            </select>
        </div>

    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">APP状态*</label>
        <div class="layui-input-block">
            <input type="checkbox" name="like[write]" title="待审核">
            <input type="checkbox" name="like[read]" title="已审核" checked>
        </div>
    </div>

    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">应用简介*</label>
        <div class="layui-input-block">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
            <button type="reset" class="layui-btn layui-btn-primary">返回</button>
        </div>
    </div>
</form>
<script src="${ctx}/statics/layui/layui.all.js"></script>
<script type="text/javascript">
    //Demo
    layui.use('form', function(){
        var form = layui.form;

        //监听提交
        form.on('submit(formDemo)', function(data){
            layer.msg(JSON.stringify(data.field));
            return false;
        });
    });
</script>
</body>
</html>
