<%--
  Created by IntelliJ IDEA.
  User: je
  Date: 2020/9/21
  Time: 9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/contextPath.jsp" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${ctx}/statics/layui/css/layui.css">
    <link rel="stylesheet" href="${ctx}/statics/css/style.css">
    <link rel="stylesheet" href="${ctx}/statics/css/font-awesome.min.css">
    <link rel="stylesheet" href="${ctx}/statics/summernote/summernote.css">
    <link rel="stylesheet" href="${ctx}/statics/summernote/summernote-bs3.css">
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .layui-form-label {
            width: 100px;
        }

        #checkAppInfo, #appVersionInfo {
            font-size: 12px;
            color: #666666;
            text-align: center;
        }

        #checkAppInfo input, #appVersionInfo input {
            height: 30px;
            width: 240px;
            line-height: 30px;
        }

        #checkAppInfo label, #appVersionInfo label {
            line-height: 10px;
        }
    </style>
</head>
<body style="margin: 0;padding: 0">
<!-- 搜索条件开始 -->
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>APP信息管理维护</legend>
</fieldset>
<form action="" class="layui-form" method="post" id="searchForm">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">软件名称:</label>
            <div class="layui-input-inline">
                <input type="text" name="softwareName" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">所属平台:</label>
            <div class="layui-input-inline">
                <select name="flatformId" lay-filter="flatform" id="flatform" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">一级分类:</label>
            <div class="layui-input-inline">
                <select name="categorylevel1" lay-filter="level1" id="level1" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">二级分类:</label>
            <div class="layui-input-inline">
                <select name="categorylevel2" lay-filter="level2" id="level2" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">三级分类:</label>
            <div class="layui-input-inline">
                <select name="categorylevel3" lay-filter="level3" id="level3" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">APP状态:</label>
            <div class="layui-input-inline">
                <select name="status" lay-filter="status" id="status" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-input-inline" style="margin-left: 20px">
                <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-search"
                        id="doSearch">查询
                </button>
                <button type="reset" class="layui-btn layui-btn-warm layui-btn-sm layui-icon layui-icon-refresh">重置
                </button>
            </div>
        </div>
    </div>
</form>

<!-- 搜索条件结束 -->
<!-- 内容主体区域 -->
<table class="layui-hide" id="appTable" lay-filter="appTable"></table>
<%--        表头工具栏--%>
<script type="text/html" id="toolbarDemo">
    <div class="layui-btn-container">
        <button class="layui-btn layui-btn-sm" lay-event="addApp" id="tes"><i class="layui-icon layui-icon-add-1"></i>新增APP基础信息
        </button>
        <button class="layui-btn layui-btn-sm" lay-event="batchDelete"><i class="layui-icon layui-icon-delete"></i>批量删除
        </button>
    </div>
</script>
<%--        表头工具栏结束--%>

<%--        行工具栏开始--%>
<div id="barDemo" class="layui-hide">
    <button class="layui-btn layui-btn-xs" id="dropdownMenu_{{d.id}}">
        <span>点击操作</span>
        <i class="layui-icon layui-icon-triangle-d"></i>
    </button>
</div>
<%--下拉框菜单渲染，使用laytpl语法实现逻辑判断--%>
<script id="myMenus" type="text/plain">
            [   [{layIcon: 'layui-icon-username', txt: '新增版本', event: 'addVersion'}]
                [{layIcon: 'layui-icon-edit', txt: '修改版本', event: 'editVersion'}]
                [{layIcon: 'layui-icon-edit', txt: '修改APP', event: 'editApp'}]
                [{layIcon: 'layui-icon-delete', txt: '删除APP', event: 'delApp'}]
                [{layIcon: 'layui-icon-file-b', txt: '查看APP', event: 'detailApp'}]
                {{#  if(d.status==4){ }}
                [{layIcon: 'layui-icon-tread', txt: '下架APP', event: 'takeDownApp'}]
                {{# } }}
                 {{#  if(d.status==5||d.status==2){ }}
                [{layIcon: 'layui-icon-upload-circle', txt: '上架APP', event: 'uploadApp'}
                {{# } }}
            ]]















</script>
<%--        行工具栏结束--%>
<%--        增改查APP信息弹出层开始--%>
<div id="checkAppInfo" style="display: none">
    <%--    app基础信息开始--%>
    <form class="layui-form" method="post" enctype="multipart/form-data" lay-filter="appInfoForm" id="appInfoForm"
          style="padding-top: 20px">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">软件名称 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required" name="softwareName" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">APK名称 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required" name="apkName" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">支持ROM <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required" name="supportRom" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">界面语言 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required" name="interfaceLanguage" autocomplete="off"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">软件大小 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required" name="softwareSize" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">下载次数 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" lay-verify="required" name="downloads" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">所属平台 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <select name="flatformId" lay-verify="required" lay-filter="flat_form" id="flat_form" lay-search="">

                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">一级分类 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <select name="categorylevel1" lay-verify="required" lay-filter="level1" id="level_1" lay-search="">

                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">二级分类 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <select name="categorylevel2" lay-verify="required" lay-filter="level2" id="level_2" lay-search="">

                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">三级分类 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <select name="categorylevel3" lay-filter="level_3" id="level_3" lay-search="">

                    </select>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">APP状态 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" value="待审核" name="statusName" id="state" autocomplete="off" disabled
                           class="layui-input">

                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">应用简介 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <textarea class="layui-textarea" lay-verify="required" name="appinfo"
                              style="min-height: 50px;min-width: 240px"></textarea>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <%--            隐藏表单 提交头像文件名字--%>
                <input type="hidden" name="status">
                <input type="hidden" name="id">
                <input type="hidden" name="logoPath" id="appLogo"/>
                <input type="hidden" name="devId" value="${sessionScope.sysUser.id}"/>
                <input type="hidden" name="createBy" value="${sessionScope.sysUser.userName}"/>
                <input type="hidden" name="modifyBy" value="${sessionScope.sysUser.userName}"/>
                <label class="layui-form-label">LOGO图片 <span style="color: red">*</span></label>
                <div class="layui-input-inline" style="text-align: left">
                    <div class="layui-upload">
                        <button type="button" class="layui-btn layui-btn-sm" id="test1"><i
                                class="layui-icon">&#xe67c;</i>上传图片
                        </button>
                        <div class="layui-upload-list">
                            <img class="layui-upload-img" id="demo1"
                                 style="width: 80px;height: 80px;border-radius: 50%"/>
                            <p id="demoText"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <button type="submit" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-ok-circle"
                    lay-submit="" lay-filter="doSubmit"
                    id="addSave">保存
            </button>
            <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-refresh-1"
                    id="back">返回
            </button>
        </div>

    </form>
    <%--    app基础信息结束--%>
</div>
<%--        增改查APP信息弹出层结束--%>

<%--查看修改APP版本信息弹出层开始--%>
<div id="appVersionInfo" style="display: none">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;text-align: left">
        <legend>历史版本列表</legend>
    </fieldset>
    <table class="layui-hide" id="appVersionTable" lay-filter="appVersionTable"></table>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;text-align: left">
        <legend id="headText">新增版本信息</legend>
    </fieldset>
    <form action="" class="layui-form" method="post" lay-filter="appVersionForm" id="appVersionForm">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">版本号 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="versionNo" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">版本大小 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="versionSize" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">发布状态 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="publishStatusName" disabled autocomplete="off" class="layui-input"
                           id="publishStatusName">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">版本简介 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <textarea class="layui-textarea" name="versionInfo"
                              style="min-height: 50px;min-width: 240px"></textarea>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <%--            隐藏表单 提交头像文件名字--%>
                <input type="hidden" name="publishStatus">
                <input type="hidden" name="id" id="versionId">
                <input type="hidden" name="createBy" value="${sessionScope.sysUser.userName}"/>
                <input type="hidden" name="modifyBy" value="${sessionScope.sysUser.userName}"/>
                <label class="layui-form-label">apk文件 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="apkFilename" autocomplete="off" class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-ok-circle"
                    id="saveVersion">保存
            </button>
            <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-refresh-1"
                    id="backTo">返回
            </button>
        </div>
    </form>
</div>
<script src="${ctx}/statics/js/jquery-3.4.1.min.js"></script>
<script src="${ctx}/statics/layui/layui.all.js"></script>
<script src="${ctx}/statics/validate/jquery.validate.min.js"></script>
<script src="${ctx}/statics/validate/messages_zh.min.js"></script>
<script src="${ctx}/statics/validate/jquery.validate.extend.js"></script>
<script type="text/javascript">
    var form = layui.form;
    var layer = layui.layer;
    var table = layui.table;
    var laydate = layui.laydate;
    var $ = layui.jquery;
    var upload = layui.upload;
    var formaction;//app信息表单提交地址
    var vformaction;//app版本信息表单提交地址
    var layerIndex;//弹层index,用于关闭弹层
    var appTable;//app信息表格


    //表单validate验证
    function validateForm() {
        return $("#appVersionForm").validate({
            onkeyup: false,
            rules: {
                versionNo: {
                    required: true,
                    minlength: 2,
                    maxlength: 20,
                },
                versionSize: {
                    required: true,
                    number: true
                },
                versionInfo: {
                    required: true,
                },
                apkFilename: {
                    required: true,
                },
                focusCleanup: true
            }
        })
    }

    //app版本form提交
    function doSubmit(data) {
        if (validateForm().form()) {
            $.ajax({
                "url": vformaction,
                "data": $("#appVersionForm").serialize() + "&appId=" + data.id,
                "type": "post",
                "dataType": "json",
                "success": function (result) {
                    //新增
                    if (result != '' && $("#versionId").val() == '') {
                        $.post("${ctx}/appInfo/update", {"id": data.id, "versionid": result}, function (obj) {
                            if (obj > 0) {
                                appTable.reload();
                                layer.close(layerIndex);
                                layer.msg("添加成功", {icon: 1, time: 2000})
                            } else {
                                layer.msg("添加失败", {icon: 1, time: 2000})
                            }
                        })
                        //    修改
                    } else if (result == "1") {
                        appTable.reload();
                        layer.close(layerIndex);
                        layer.msg("修改成功", {icon: 1, time: 2000})
                    } else {
                        layer.msg("修改失败", {icon: 1, time: 2000})
                    }
                }
            })
            return false;
        }
    }

    layui.config({
        base: '${ctx}/statics/layui/dropdown/'
    }).use(['dropdown'], function () {
        //渲染table数据表格
        var dropdown = layui.dropdown;
        //渲染表格
        appTable = table.render({
            elem: '#appTable'
            , id: 'appList'
            , url: '${ctx}/appInfo/list?status=1'
            , page: true
            , toolbar: "#toolbarDemo"
            , cellMinWidth: 100 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            , defaultToolbar: ['filter', 'print']
            , cols: [[
                {type: "checkbox", align: "center", width: "5%", id: 'checkTest'}
                , {field: 'softwareName', title: '软件名称', align: 'center', width: "10%"} //width 支持：数字、百分比和不填写。你还可以通过 minWidth 参数局部定义当前单元格的最小宽度，layui 2.2.1 新增
                , {field: 'apkName', title: 'APK名称', align: 'center', width: "10%"}
                , {
                    field: 'softwareSize', title: '软件大小(M)', align: "center", width: "10%"
                }
                , {
                    field: 'flatformName', title: '所属平台', align: 'center', width: "8%"
                }
                //单元格内容水平居中
                , {
                    field: 'categorylevel1Name', title: '一级分类', align: 'center', width: "8%"
                }, {
                    field: 'categorylevel2Name', title: '二级分类', align: 'center', width: "8%"
                }, {
                    field: 'categorylevel3Name', title: '三级分类', align: 'center', width: "8%"
                }, {
                    field: 'status', title: '状态', align: 'center', width: "9%", templet: function (data) {
                        var statusName;
                        if (data.status == "1") {
                            statusName = "待审核"
                        } else if (data.status == "2") {
                            statusName = "审核通过";
                        } else if (data.status == "3") {
                            statusName = "审核未通过";
                        } else if (data.status == "4") {
                            statusName = "已上架";
                        } else if (data.status == "5") {
                            statusName = "已下架";
                        }
                        return statusName;
                    }
                }
                , {
                    field: 'downloads', title: '下载次数', align: 'center', width: "7%"
                }, {
                    field: 'versionNo', title: '最新版本号', align: 'center', width: "8%"
                }
                , {title: "操作", width: "9%", align: 'center', toolbar: '#barDemo'}
            ]],
            //表格渲染完成，加载每行工具栏下拉菜单
            done: function (res) {
                for (var i = 0; i < res.data.length; i++) {
                    //渲染下拉菜单
                    dropdown.suite("#dropdownMenu_" + res.data[i].id, {
                        data: res.data[i],
                        templateMenu: '#myMenus',
                        align: 'right'
                    });
                }
            }
        });
        //监听表头工具栏事件
        table.on('toolbar(appTable)', function (obj) {
            var layEvent = obj.event;
            if (layEvent == "addApp") {
                manageApp(null);
                $("#appInfoForm")[0].reset();
                $("#demo1").removeAttr("src");
                formaction = "${ctx}/appInfo/addSave";
            }
        })
        //    监听行工具栏单击事件
        table.on('tool(appTable)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            const data = obj.data; //获得当前行数据
            const layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            let status = data.status;
            //重新启用form表单
            $("#appInfoForm").find(":input[name]").each(function () {
                $(this).removeAttr("disabled", "true");
            });
            $("#appInfoForm button").show();
            switch (layEvent) {
                case "addVersion":
                    vformaction = "${ctx}/appVersion/addSave";
                    manageAppVersion(data, 1);
                    break;
                case "editVersion":
                    var version = data.versionNo;
                    if (version == "") {
                        layer.msg("该APP无版本信息，请先上传版本信息", {icon: 2})
                        return false;
                    }
                    if (status == 2) {
                        layer.msg("该APP已审核通过，不可修改版本信息", {icon: 2})
                        return false;
                    }
                    if (status == 4) {
                        layer.msg("该APP已上架，不可修改版本信息", {icon: 2})
                        return false;
                    }
                    if (status == 5) {
                        layer.msg("该APP已下架，不可修改版本信息", {icon: 2})
                        return false;
                    }
                    vformaction = "${ctx}/appVersion/update";
                    manageAppVersion(data, 2);
                    form.render();
                    break;
                case "editApp":
                    if (status == 2) {
                        layer.msg("该APP已审核通过，不可修改信息", {icon: 2})
                        return false;
                    }
                    if (status == 4) {
                        layer.msg("该APP已上架，不可修改信息", {icon: 2})
                        return false;
                    }
                    if (status == 5) {
                        layer.msg("该APP已下架，不可修改信息", {icon: 2})
                        return false;
                    }
                    formaction = "${ctx}/appInfo/update";
                    manageApp(data);
                    break;
                case "delApp":
                    formaction = "${ctx}/appInfo/delete/" + data.id;
                    deleteApp(data);
                    break;
                case "detailApp":
                    formaction = "${ctx}/appInfo/detail/" + data.id;
                    manageApp(data);
                    //查看信息不可编辑
                    $("#appInfoForm").find(":input[name]").each(function () {
                        $(this).attr("disabled", "true");
                    });
                    $("#appInfoForm button").hide();
                    break;
                case "uploadApp":
                    uploadApp(data);
                    break;
                case "takeDownApp":
                    takeDownApp(data);
                    break;
            }
            var statusName;
            if (status == "1") {
                statusName = "待审核";
            } else if (status == "2") {
                statusName = "审核通过";
            } else if (status == "3") {
                statusName = "审核未通过";
            } else if (status == "4") {
                statusName = "已上架";
            } else if (status == "5") {
                statusName = "已下架";
            }
            $("#state").val(statusName);
            form.render('select');
        });


        //弹出层显示APP信息
        function manageApp(data) {
            layerIndex = layer.open({
                type: 1,
                title: data == null ? '新增APP基础信息' : '修改APP基础信息',
                content: $("#checkAppInfo"),
                area: ['520px', '620px'],
                offset: '5px',
                success: function () {
                    $("#state").val("待审核");
                    //返回按钮
                    $("#back").click(function () {
                        layer.close(layerIndex);
                        appTable.reload();
                    })
                    // 表格赋值
                    if (data != null) {
                        form.val("appInfoForm", data);
                        $("input[name='createBy']").val("");
                        $("input[name='modifyBy']").val("${sessionScope.sysUser.userName}");
                        $("#demo1").attr("src", "${ctx}/statics/images/" + data.logoPath);
                    } else {
                        $("input[name='modifyBy']").val("");
                        $("input[name='createBy']").val("${sessionScope.sysUser.userName}")
                        $("input[name='id']").removeAttr("value");
                    }

                    //重新渲染表格
                    form.render();
                }
            })
        }

        //弹层显示APP版本信息
        function manageAppVersion(data, index) {
            layerIndex = layer.open({
                type: 1,
                title: index == 1 ? '新增APP版本信息' : '修改APP版本信息',
                content: $("#appVersionInfo"),
                area: ['920px', '520px']
                , cellMinWidth: 100 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
                , success: function () {
                    $("#saveVersion").click(function () {
                        doSubmit(data);
                    });
                    $("#backTo").click(function () {
                        appTable.reload();
                        layer.close(layerIndex);
                    })
                    $("#appVersionForm")[0].reset();
                    $("#publishStatusName").val("预发布");
                    table.render({
                        elem: '#appVersionTable'
                        , id: 'versionList'
                        , url: '${ctx}/appVersion/list/' + data.id
                        , cols: [[
                            {
                                field: 'softwareName',
                                title: '软件名称',
                                align: 'center',
                                width: "17%",
                                templet: function () {
                                    return data.softwareName
                                }
                            } //width 支持：数字、百分比和不填写。你还可以通过 minWidth 参数局部定义当前单元格的最小宽度，layui 2.2.1 新增
                            , {field: 'versionNo', title: '版本号', align: 'center', width: "15%"}
                            , {field: 'versionSize', title: '版本大小(M)', align: "center", width: "14%"}
                            , {field: 'publishStatusName', title: '发布状态', align: 'center', width: "15%"}
                            , {field: 'apkFilename', title: 'APK文件下载', align: 'center', width: "20%"}
                            , {field: 'modifyTime', title: '最近更新时间', align: 'center', width: "17%"}
                        ]]
                    })
                    // 表格赋值
                    if (index == 1) {
                        vformaction = "${ctx}/appVersion/addSave";
                        $("input[name='modifyBy']").removeAttr("value");
                        $("input[name='createBy']").val("${sessionScope.sysUser.userName}");
                        $("input[name='id']").removeAttr("value");
                        $("input[name='publishStatus']").val("3");
                    } else {
                        vformaction = "${ctx}/appVersion/update";
                        $.post("${ctx}/appVersion/getVersion/" + data.versionid, function (result) {
                            form.val('appVersionForm', JSON.parse(result))
                        })
                        $("#headText").html("修改版本信息");
                        $("input[name='createBy']").removeAttr("value");
                        $("input[name='modifyBy']").val("${sessionScope.sysUser.userName}")
                    }
                    form.render();
                }
            });
        }

        //上架app
        function uploadApp(data) {
            layer.confirm("确认上架此APP吗？", {
                icon: 3,
                yes: function () {
                    $.post("${ctx}/appInfo/update?status=4&id=" + data.id, function (result) {
                        if (result > 0) {
                            appTable.reload();
                            layer.msg("上架成功！", {icon: 1})
                        } else {
                            layer.msg("上架失败！请稍后重试", {icon: 2})
                        }
                    })
                    return false;
                }
            })
        }

        //下架app
        function takeDownApp(data) {
            layer.confirm("确认下架此APP吗？", {
                icon: 3,
                yes: function () {
                    $.post("${ctx}/appInfo/update?status=5&id=" + data.id, function (result) {
                        if (result > 0) {
                            appTable.reload();
                            layer.msg("下架成功！", {icon: 1})
                        } else {
                            layer.msg("下架失败！请稍后重试", {icon: 2})
                        }
                    })
                    return false;
                }
            })
        }

        //appLOGO图片上传
        var uploadInst = upload.render({
            elem: '#test1'
            , url: '${ctx}/appInfo/upload' //改成您自己的上传接口
            , size: 200 //限制文件大小，单位 KB
            , accept: "images" //指定图片格式
            , before: function (obj) {
                //预读本地文件示例，不支持ie8
                obj.preview(function (index, file, result) {
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            }
            , done: function (res) {
                //上传成功
                if (res.code == 0) {
                    $("#appLogo").val(res.fileName);
                    layer.msg('上传成功', {icon: 1});
                } else {
                    //如果上传失败
                    layer.msg('上传失败');
                }
            }
            , error: function () {
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-xs demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function () {
                    uploadInst.upload();
                });
            }
        });

        //删除APP信息
        function deleteApp(data) {
            var flag = layer.confirm("确认删除此APP信息吗？", {
                icon: 3,
                yes: function () {
                    $.post("${ctx}/appInfo/delete/" + data.id, function (result) {
                        if (result > 0) {
                            appTable.reload();
                            layer.msg("删除成功！", {icon: 1})
                        } else {
                            layer.msg("删除失败！请稍后重试", {icon: 2})
                        }
                    })
                    return false;
                }
            })
        }

        //监听表单提交事件
        //新增，修改，查看app信息的ajax请求
        form.on('submit(doSubmit)', function () {
            $.ajax({
                "url": formaction,
                "data": $("#appInfoForm").serialize(),
                "dataType": "json",
                "type": "post",
                "success": function (obj) {
                    if (obj == "1") {
                        layer.close(layerIndex);//关闭弹层
                        appTable.reload();//刷新表格数据
                        layer.msg('操作成功！', {icon: 1, time: 2000})
                    } else {
                        layer.msg('操作失败！', {icon: 2, time: 2000})
                        return false;
                    }
                }, "error": function () {
                    layer.msg('数据接口请求失败！', {icon: 2, time: 2000})
                }
            })
            return false;
        })

        // 条件查询列表
        $("#doSearch").click(function () {
            var softwareName = $("#searchForm input[name='softwareName']").val();
            var flatform = $("#flatform  option:selected").val();
            var level1 = $("#level1  option:selected").val();
            var level2 = $("#level2  option:selected").val();
            var level3 = $("#level3  option:selected").val();
            var status = $("#status option:selected").val();
            appTable.reload({
                url: '${ctx}/appInfo/list',
                where: {
                    "softwareName": softwareName,
                    "flatformId": flatform,
                    "categorylevel1": level1,
                    "categorylevel2": level2,
                    "categorylevel3": level3,
                    "status": status
                }
            });
        })

        /**
         * 动态加载三级菜单
         * @param data 参数
         * @param selector 填充id选择器
         */
        function initLevelCategory(data, selector) {
            $.ajax({
                "url": "${ctx}/appCategory/level",
                "data": data,
                "dataType": "json",
                "type": "post",
                "success": function (data) {
                    var $level = $(selector).empty();
                    $level.append($("<option value=''>请选择</option>"))
                    for (var i = 0; i < data.length; i++) {
                        $level.append($("<option value='" + data[i].id + "'>" + data[i].categoryName + "</option>"));
                    }
                    form.render();
                },
                "error": function () {
                    layer.msg("初始化菜单失败！", {icon: 2, time: 2000})
                }
            })
        }

        //初始化字典信息
        function initDictionary(data, selector) {
            $.ajax({
                "url": "${ctx}/dict/list",
                "data": data,
                "dataType": "json",
                "type": "post",
                "success": function (data) {
                    var $flat = $(selector).empty();
                    $flat.append($("<option value=''>请选择</option>"))
                    for (var i = 0; i < data.length; i++) {
                        $flat.append($("<option value='" + data[i].valueId + "'>" + data[i].valueName + "</option>"));
                    }
                    //APP状态默认显示审核
                    if (selector === "#status") {
                        $(selector).val(1);
                    }
                    form.render();
                },
                "error": function () {
                    layer.msg("初始化平台失败！", {icon: 2, time: 2000})
                }
            })
        }

        //页面加载时初始化下拉菜单信息
        $(function () {
            //初始化平台信息
            initDictionary({"typeCode": "APP_FLATFORM"}, "[name='flatformId']");
            //初始化APP状态信息
            initDictionary({"typeCode": "APP_STATUS"}, "#status");
            //初始化加载一级菜单
            initLevelCategory({"parentid": 0}, "[name='categorylevel1']");
            //动态加载二级菜单
            form.on('select(level1)', function (data) {
                initLevelCategory({"parentid": data.value}, "[name='categorylevel2']");
            })
            //动态加载三级菜单
            form.on('select(level2)', function (data) {
                initLevelCategory({"parentid": data.value}, "[name='categorylevel3']");
            })
        })
    })
</script>
</body>
</html>
