<%--
  Created by IntelliJ IDEA.
  User: 贾松泊
  Date: 2020/10/20
  Time: 14:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/contextPath.jsp" %>
<head>
    <meta charset="utf-8">
    <title>新增APP版本信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${ctx}/statics/layui/css/layui.css">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>

<table class="layui-hide" id="test"></table>


<script src="${ctx}/statics/layui/layui.all.js"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->

<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#test'
            ,url:'/demo/table/user/'
            ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            ,cols: [[
                {field:'', width:150, title: '软件名称'}
                ,{field:'', width:150, title: '版本号'}
                ,{field:'', width:250, title: '版本大小（单位M）'}
                ,{field:'', width:150, title: '发布状态'} //minWidth：局部定义当前单元格的最小宽度，layui 2.2.1 新增
                ,{field:'', width:450, title: 'APK文件下载'}
                ,{field:'', width:150, title: '更新下载'}
            ]]
        });
    });
</script>
<form class="layui-form" action="">
    <div class="layui-form-item">
        <label class="layui-form-label">版本号*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入软件名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">版本大小*</label>
        <div class="layui-input-block">
            <input type="text" name="title" required  lay-verify="required" placeholder="请输入APK名称" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">发布状态*</label>
        <div class="layui-input-block">
            <input type="checkbox" name="like[write]" title="预发布">

        </div>
    </div>

    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">版本简介*</label>
        <div class="layui-input-block">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"></textarea>
        </div>
    </div>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
        <legend>APK文件*</legend>
    </fieldset>

    <div class="layui-upload">
        <button class="layui-btn layui-btn-normal" id="test8" type="button">选择文件</button>
        <button class="layui-btn" id="test9" type="button">开始上传</button>
    </div>
    <legend></legend>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
            <button type="reset" class="layui-btn layui-btn-primary">返回</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    upload.render({
        elem: '#test8'
        ,url: 'https://httpbin.org/post' //改成您自己的上传接口
        ,auto: false
        //,multiple: true
        ,bindAction: '#test9'
        ,done: function(res){
            layer.msg('上传成功');
            console.log(res)
        }
    });
</script>

</body>
</html>