<%--
  Created by IntelliJ IDEA.
  User: je
  Date: 2020/9/21
  Time: 9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../common/contextPath.jsp" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="${ctx}/statics/layui/css/layui.css">
    <link rel="stylesheet" href="${ctx}/statics/css/style.css">
    <link rel="stylesheet" href="${ctx}/statics/css/font-awesome.min.css">
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        .layui-form-label {
            width: 100px;
        }

        #checkAppInfo {
            font-size: 12px;
            color: #666666;
            text-align: center;
        }

        #checkAppInfo input {
            height: 30px;
            width: 240px;
            line-height: 30px;
        }

        #checkAppInfo label {
            line-height: 10px;
        }
    </style>
</head>
<body style="margin: 0;padding: 0">
<!-- 搜索条件开始 -->
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>APP审核列表</legend>
</fieldset>
<form action="" class="layui-form" method="post" id="searchForm">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">软件名称:</label>
            <div class="layui-input-inline">
                <input type="text" name="softwareName" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">所属平台:</label>
            <div class="layui-input-inline">
                <select name="flatform" lay-filter="flatform" id="flatform" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">一级分类:</label>
            <div class="layui-input-inline">
                <select name="level1" lay-filter="level1" id="level1" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">二级分类:</label>
            <div class="layui-input-inline">
                <select name="level2" lay-filter="level2" id="level2" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">三级分类:</label>
            <div class="layui-input-inline">
                <select name="level3" lay-filter="level3" id="level3" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">APP状态:</label>
            <div class="layui-input-inline">
                <select name="status" lay-filter="status" id="status" lay-search="">

                </select>
            </div>
        </div>
        <div class="layui-inline">
            <div class="layui-input-inline" style="margin-left: 20px">
                <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-search"
                        id="doSearch">查询
                </button>
                <button type="reset" class="layui-btn layui-btn-warm layui-btn-sm layui-icon layui-icon-refresh">重置
                </button>
            </div>
        </div>
    </div>
</form>

<!-- 搜索条件结束 -->
<!-- 内容主体区域 -->
<table class="layui-hide" id="appTable" lay-filter="appTable"></table>
<%--        行工具栏开始--%>
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="check">审核</a>
</script>
<%--        行工具栏结束--%>
<%--        弹出层开始--%>
<div id="checkAppInfo" style="display: none">
    <%--    app基础信息开始--%>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>APP基础信息</legend>
    </fieldset>
    <form action="" class="layui-form" method="post" lay-filter="appInfoForm" id="appInfoForm">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">软件名称 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="softwareName" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">APK名称 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="apkName" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">支持ROM <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="supportRom" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">界面语言 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="interfaceLanguage" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">软件大小 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="softwareSize" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">下载次数 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="downloads" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">所属平台 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="flatformName" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">一级分类 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="categorylevel1Name" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">二级分类 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="categorylevel2Name" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">三级分类 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="categorylevel3Name" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">APP状态 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="status" id="state" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">应用简介 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <textarea class="layui-textarea" name="appinfo" disabled
                              style="min-height: 50px;min-width: 240px"></textarea>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-ok-circle"
                    id="check" >审核通过
            </button>
            <button type="button" class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-close-fill"
                    id="notCheck">审核不通过
            </button>
            <button type="button"  class="layui-btn layui-btn-normal layui-btn-sm layui-icon layui-icon-refresh-1"
                    id="back">返回
            </button>
        </div>

    </form>
    <%--    app基础信息结束--%>
    <%--    app最新版本信息开始--%>
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend>最新版本信息</legend>
    </fieldset>
    <form action="" class="layui-form" method="post" lay-filter="appVersionForm" id="appVersionForm">
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">最新版本号 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="versionNo" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">版本大小 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="versionSize" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">发布状态 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="publishStatusName" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">版本简介 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <textarea class="layui-textarea" name="versionInfo" disabled
                              style="min-height: 50px;min-width: 240px"></textarea>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">apk文件 <span style="color: red">*</span></label>
                <div class="layui-input-inline">
                    <input type="text" name="apkFilename" autocomplete="off" disabled class="layui-input">
                </div>
            </div>
        </div>
    </form>
    <%--    app最新版本信息结束--%>

</div>
<%--        弹出层结束--%>

<script src="${ctx}/statics/js/jquery-3.4.1.min.js"></script>
<script src="${ctx}/statics/validate/jquery.min.js"></script>
<script src="${ctx}/statics/layui/layui.all.js"></script>
<script src="${ctx}/statics/validate/jquery.validate.min.js"></script>
<script src="${ctx}/statics/validate/messages_zh.min.js"></script>
<script src="${ctx}/statics/validate/jquery.validate.extend.js"></script>
<script>
    //渲染table数据表格
    var form = layui.form;
    var layer = layui.layer;
    var table = layui.table;
    var laydate = layui.laydate;

    //渲染表格
    var appTable = table.render({
        elem: '#appTable'
        , id: 'appList'
        , url: '${ctx}/appInfo/list?status=1'
        , page: true
        , toolbar: "#toolbarDemo"
        , cellMinWidth: 100 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        , defaultToolbar: ['filter', 'print']
        , cols: [[
            {type: "checkbox", align: "center", width: "5%", id: 'checkTest'}
            , {field: 'softwareName', title: '软件名称', align: 'center', width: "13%"} //width 支持：数字、百分比和不填写。你还可以通过 minWidth 参数局部定义当前单元格的最小宽度，layui 2.2.1 新增
            , {field: 'apkName', title: 'APK名称', align: 'center', width: "13%"}
            , {
                field: 'softwareSize', title: '软件大小(M)', align: "center", width: "10%"
            }
            , {
                field: 'flatformName', title: '所属平台', align: 'center', width: "9%"
            }
            //单元格内容水平居中
            , {
                field: 'categorylevel1Name', title: '一级分类', align: 'center', width: "8%"
            }, {
                field: 'categorylevel2Name', title: '二级分类', align: 'center', width: "8%"
            }, {
                field: 'categorylevel3Name', title: '三级分类', align: 'center', width: "8%"
            }
            , {
                field: 'downloads', title: '下载次数', align: 'center', width: "10%"
            }, {
                field: 'versionNo', title: '最新版本号', align: 'center', width: "8%"
            }
            , {title: "操作", width: "8%", align: 'center', toolbar: '#barDemo'}
        ]]

    });
    //    监听行工具栏单击事件
    table.on('tool(appTable)', function (obj) { //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
        const data = obj.data; //获得当前行数据
        const layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）

        if (data.status == "2") {
            layer.msg("该APP已审核通过，不能进行审核操作！", {icon: 2})
            return false;
        }
        if (data.status == "3") {
            layer.msg("该APP审核未通过，请更新版本后重试！", {icon: 2})
            return false;
        }
        if (data.status == "4") {
            layer.msg("该APP已上架，不能进行审核操作！", {icon: 2})
            return false;
        }
        if (data.status == "5") {
            layer.msg("该APP已下架，不能进行审核操作！", {icon: 2})
            return false;
        }
        if (data.versionNo === "") {
            layer.msg("该APP未上传最新版本，不能进行审核操作！", {icon: 2})
            return false;
        }
        if (layEvent === 'check') { //查看
            checkApp(data);
        }
    });
    var layerIndex;
    //弹出层显示APP信息
    function checkApp(data) {
        layerIndex = layer.open({
            type: 1,
            title: '查看并审核APP信息',
            content: $("#checkAppInfo"),
            area: ['520px', '600px'],
            success: function () {
                form.val("appInfoForm", data);
                $("#state").val("待审核");
                //ajax查询APP最新版本信息
                $.ajax({
                    "url": "${ctx}/appVersion/getVersion/" + data.versionid,
                    "type": "post",
                    "dataType": "json",
                    "success": function (result) {
                        form.val("appVersionForm", result);
                    },
                    "error": function () {
                        layer.msg("初始化版本信息失败", {icon: 2});
                    }
                })
                //审核通过
                $("#check").click(function () {
                    updateApp(data.id,2);
                })
                //审核不通过
                $("#notCheck").click(function () {
                    updateApp(data.id,3);
                })
                //返回按钮
                $("#back").click(function () {
                    layer.close(layerIndex);
                    appTable.reload();
                })
                //重新渲染表格
                form.render();
            }
        })
    }

    /**
     * app审核
     *  @param id 审核APP的id
     * @param status 审核状态 是否通过
     */

    function updateApp(id, status) {
        $.ajax({
            "url":"${ctx}/appInfo/update",
            "data":{"id":id,"status":status},
            "dataType":"json",
            "type":"post",
            "success":function (result) {
                if (result=="1"){
                    layer.msg("审核成功！",{icon:1,time:2000});
                    layer.close(layerIndex);
                    appTable.reload();
                }else {
                    layer.msg("审核失败,请稍后重试！",{icon:2,time:2000})
                }
            },"error":function () {
                layer.msg("出现未知错误,请稍后重试！",{icon:2,time:2000})
            }
        })
        return false;
    }

    // 监听业务表单提交事件
    $("#doSearch").click(function () {
        var softwareName = $("#searchForm input[name='softwareName']").val();
        var flatform = $("#flatform  option:selected").val();
        var level1 = $("#level1  option:selected").val();
        var level2 = $("#level2  option:selected").val();
        var level3 = $("#level3  option:selected").val();
        var status = $("#status option:selected").val();
        appTable.reload({
            url: '${ctx}/appInfo/list',
            where: {
                "softwareName": softwareName,
                "flatformId": flatform,
                "categorylevel1": level1,
                "categorylevel2": level2,
                "categorylevel3": level3,
                "status": status
            }
        });
    })
    //初始化下拉菜单信息
    $(function () {
        //初始化字典信息
        function initDictionary(data, selector) {
            $.ajax({
                "url": "${ctx}/dict/list",
                "data": data,
                "dataType": "json",
                "type": "post",
                "success": function (data) {
                    var $flat = $(selector).empty();
                    $flat.append($("<option value=''>请选择</option>"))
                    for (var i = 0; i < data.length; i++) {
                        $flat.append($("<option value='" + data[i].valueId + "'>" + data[i].valueName + "</option>"));
                    }
                    //APP状态默认显示审核
                    if (selector === "#status") {
                        $(selector).val(1);
                    }
                    form.render();
                },
                "error": function () {
                    layer.msg("初始化平台失败！", {icon: 2, time: 2000})
                }
            })
        }

        //初始化平台信息
        initDictionary({"typeCode": "APP_FLATFORM"}, "#flatform");

        //初始化APP状态信息
        initDictionary({"typeCode": "APP_STATUS"}, "#status");

        /**
         * 动态加载三级菜单
         * @param data 参数
         * @param selector 填充id选择器
         */
        function initLevelCategory(data, selector) {
            $.ajax({
                "url": "${ctx}/appCategory/level",
                "data": data,
                "dataType": "json",
                "type": "post",
                "success": function (data) {
                    var $level = $(selector).empty();
                    $level.append($("<option value=''>请选择</option>"))
                    for (var i = 0; i < data.length; i++) {
                        $level.append($("<option value='" + data[i].id + "'>" + data[i].categoryName + "</option>"));
                    }
                    form.render();
                },
                "error": function () {
                    layer.msg("初始化菜单失败！", {icon: 2, time: 2000})
                }
            })
        }

        //初始化加载一级菜单
        initLevelCategory({"parentid": 0}, "#level1");
        //动态加载二级菜单
        form.on('select(level1)', function (data) {
            initLevelCategory({"parentid": data.value}, "#level2");
        })
        //动态加载三级菜单
        form.on('select(level2)', function (data) {
            initLevelCategory({"parentid": data.value}, "#level3");
        })
    })


</script>
</body>
</html>
