<%--
  Created by IntelliJ IDEA.
  User: je
  Date: 2020/10/16
  Time: 22:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../../common/contextPath.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon"
          href="http://www.weather.com.cn/favicon.ico" type="image/x-icon" />
    <title>APP管理者平台</title>
    <link rel="stylesheet" href="${ctx}/statics/layui/css/layui.css">
    <style>
        #body {
            overflow: hidden;
        }

        #iframeBox {
            width: 100%;
            border: 0;
            height: 100%;
            padding: 0;
        }

        .user-photo {
            width: 200px;
            height: 120px;
            padding: 15px 0 5px;
        }

        .user-photo a.img {
            display: block;
            width: 80px;
            height: 80px;
            margin: 0 auto 10px;
        }

        .user-photo a.img img {
            display: block;
            width: 100%;
            height: 100%;
            border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border: 4px solid #44576b;
            box-sizing: border-box;
        }

        .user-photo p {
            display: block;
            width: 100%;
            height: 25px;
            color: #ffffff;
            text-align: center;
            font-size: 12px;
            white-space: nowrap;
            line-height: 25px;
            overflow: hidden;
        }

        .component {
            width: 180px;
            height: 30px;
            margin: 0 auto 5px;
            position: relative;
            left: -10px;
        }

        .component .layui-input {
            height: 30px;
            color: white;
            line-height: 30px;
            font-size: 12px;
            border: none;
            transition: all 0.3s;
            padding-right: 30px;
            background: rgba(255, 255, 255, 0.05);
        }

        .component .layui-icon {
            position: absolute;
            right: 8px;
            top: 8px;
            color: #000;
            cursor: pointer;
        }

        #menu_list i {
            font-size: 16px;
            margin-right: 10px;
            font-style: normal;
            -webkit-font-smoothing: antialiased;
        }

        .tab-items dd {
            padding-left: 20px;
        }
    </style>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">APP BMS</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item "><a href="">APP管理</a></li>
            <li class="layui-nav-item"><a href="">用户管理</a></li>
            <li class="layui-nav-item"><a href="">基础数据维护</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>
        <!-- 顶部右侧菜单 -->
        <ul class="layui-nav top_menu layui-layout-right">
            <li class="layui-nav-item" pc>
                <a href="javascript:;" class="clearCache"><i class="layui-icon" data-icon="&#xe640;">&#xe640;</i><cite>清除缓存</cite><span
                        class="layui-badge-dot"></span></a>
            </li>
            <li class="layui-nav-item lockcms" pc>
                <a href="javascript:;"><i class="seraph icon-lock"></i><cite>锁屏</cite></a>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;"><img src="${ctx}/statics/layui/images/恒星科技logo.png"
                                            class="layui-nav-img userAvatar" width="35" height="35"><cite
                        class="adminName">${sessionScope.user.name}</cite></a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;" data-url="page/user/userInfo.html"><i class="seraph icon-ziliao"
                                                                                     data-icon="icon-ziliao"></i><cite>个人资料</cite></a>
                    </dd>
                    <dd><a href="javascript:;" data-url="page/user/changePwd.html"><i class="seraph icon-xiugai"
                                                                                      data-icon="icon-xiugai"></i><cite>修改密码</cite></a>
                    </dd>
                    <dd><a href="javascript:;" class="showNotice"><i
                            class="layui-icon">&#xe645;</i><cite>系统公告</cite><span class="layui-badge-dot"></span></a>
                    </dd>
                    <dd pc><a href="javascript:;" class="functionSetting"><i
                            class="layui-icon">&#xe620;</i><cite>功能设定</cite><span class="layui-badge-dot"></span></a>
                    </dd>
                    <dd pc><a href="javascript:;" class="changeSkin"><i class="layui-icon">&#xe61b;</i><cite>更换皮肤</cite></a>
                    </dd>
                    <dd><a href="${ctx}/user/logOut" class="signOut"><i
                            class="seraph icon-tuichu"></i><cite>退出</cite></a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <%--左侧菜单栏开始--%>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <div class="user-photo">
                <a class="img" title="我的头像"><img
                        src="${ctx}/statics/layui/images/恒星科技logo.png"></a>
                <p>你好！<span class="userName">${sessionScope.user.name}</span>, 欢迎登录</p>
            </div>
            <!-- 搜索 -->
            <div class="layui-form component">
                <input type="text" class="layui-input" name="creatDate" id="date" autocomplete="off"
                       placeholder="搜索页面功能">
                <i class="layui-icon">&#xe615;</i>
            </div>
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test" id="menu_list">
                <li class="layui-nav-item layui-nav-itemed ">
                    <a class="" href="javascript:;"><i class="layui-icon layui-icon-cellphone-fine"></i>APP管理</a>
                    <dl class="layui-nav-child tab-items">
                        <dd><a href="javascript:;" src="${ctx}/user/admin/appCheck"><i class="layui-icon layui-icon-ok-circle"></i>APP审核</a></dd>
                        <dd><a href="javascript:;"><i class="layui-icon layui-icon-cellphone-fine"></i>APP信息</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;"><i class="layui-icon layui-icon-user"></i>用户管理</a>
                    <dl class="layui-nav-child tab-items">
                        <dd><a href="javascript:; " src="${ctx}/user/toList"><i
                                class="layui-icon layui-icon-username"></i>用户信息</a></dd>
                        <dd><a href="javascript:;"><i class="layui-icon layui-icon-cellphone-fine"></i>列表二</a></dd>
                        <dd><a href=""><i class="layui-icon layui-icon-cellphone-fine"></i>超链接</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;"><i class="layui-icon layui-icon-set"></i>基础设置</a>
                    <dl class="layui-nav-child tab-items">
                        <dd><a href="javascript:; " src="${ctx}/user/toList"><i
                                class="layui-icon layui-icon-cellphone-fine"></i>用户信息</a></dd>
                        <dd><a href="javascript:;"><i class="layui-icon layui-icon-cellphone-fine"></i>列表二</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <%--    左侧菜单栏结束--%>
    <%--    内容主题区域--%>
    <div class="layui-body" id="body">
        <iframe src="" frameborder="0" id="iframeBox">

        </iframe>
    </div>
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="${ctx}/statics/layui/layui.all.js"></script>
<script>
    //JavaScript代码区域
    layui.use('element', function () {
        var element = layui.element;
    });
</script>
<script>
    var $ = layui.jquery;
    $(".layui-nav-child dd a").click(function () {
        var target = $(this).attr("src");
        $("iframe").attr("src", target);
    });

</script>
<script type="text/javascript">
    function reinitIframe() {
        var iframe = document.getElementById("test");
        try {
            var bHeight = iframe.contentWindow.document.body.scrollHeight;
            var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
            var height = Math.max(bHeight, dHeight);
            iframe.height = height;
            console.log(height);
        } catch (ex) {
        }
    }
    window.setInterval("reinitIframe()", 200);
</script>
</body>
</html>