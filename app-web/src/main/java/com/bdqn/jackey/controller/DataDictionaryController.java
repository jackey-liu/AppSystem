package com.bdqn.jackey.controller;

import com.bdqn.jackey.entity.DataDictionary;
import com.bdqn.jackey.service.DataDictionaryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description：(DataDictionary)表控制层
 * @author：jackey
 * @create：
 */
@Controller
@RequestMapping("/dict")
public class DataDictionaryController extends BaseController{

    @Resource
    private DataDictionaryService dataDictionaryService;

    /**
     * 根据条件查询字典
     * @param dataDictionary 参数列表
     * @return 结果集合
     */
    @RequestMapping("/list")
    @ResponseBody
    public List<DataDictionary> getTypeDicts(DataDictionary dataDictionary) {
        int count = dataDictionaryService.queryAllCount(dataDictionary);
        return dataDictionaryService.queryAllDataDictionarys(dataDictionary,1,count);
    }
}