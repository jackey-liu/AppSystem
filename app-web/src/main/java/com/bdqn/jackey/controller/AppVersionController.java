package com.bdqn.jackey.controller;

import com.bdqn.jackey.entity.AppInfo;
import com.bdqn.jackey.entity.AppVersion;
import com.bdqn.jackey.service.AppVersionService;
import com.bdqn.jackey.utils.LayData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @description：(AppVersion)表控制层
 * @author：jackey
 * @create：
 */
@Controller
@RequestMapping("/appVersion")
public class AppVersionController extends BaseController {

    @Resource
    private AppVersionService appVersionService;

    /**
     * 根据id查询APP版本信息
     *
     * @param id id
     * @return APP版本信息
     */
    @RequestMapping("/getVersion/{id}")
    @ResponseBody
    public AppVersion getVersionById(@PathVariable Long id) {
        return appVersionService.queryAppVersionById(id);
    }

    /**
     * 根据appid查看APP版本信息
     *
     * @param id APPID
     * @return 版本列表
     */
    @RequestMapping("/list/{id}")
    @ResponseBody
    public LayData getVersionsByAppId(@PathVariable("id") Long id) {
        return LayData.getData(0, appVersionService.getVersionsByAppId(id));
    }

    @RequestMapping("/addSave")
    @ResponseBody
    public Long addSave(AppVersion appVersion) {
        appVersionService.insertSelective(appVersion);
        return appVersion.getId();
    }

    @RequestMapping("/update")
    @ResponseBody
    public Integer update(AppVersion appVersion) {
        return appVersionService.updateAppVersion(appVersion);
    }

}