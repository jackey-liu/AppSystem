package com.bdqn.jackey.controller;

import com.bdqn.jackey.entity.AppCategory;
import com.bdqn.jackey.service.AppCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description：(AppCategory)表控制层
 * @author：jackey
 * @create：
 */
@Controller
@RequestMapping("/appCategory")
public class AppCategoryController extends BaseController{

    @Resource
    private AppCategoryService appCategoryService;

    /**
     * 联级分类查询
     * @param appCategory 查询条件
     * @return 结果
     */
    @RequestMapping("/level")
    @ResponseBody
    public List<AppCategory> getCategoryLevel(AppCategory appCategory) {
        int count = appCategoryService.queryAllCount(appCategory);
        return appCategoryService.queryAllAppCategorys(appCategory, 1, count);
    }

}