package com.bdqn.jackey.controller;

import com.bdqn.jackey.entity.SysUser;
import com.bdqn.jackey.service.SysUserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * @description：(SysUser)表控制层
 * @author：jackey
 * @create：
 */
@Controller
@RequestMapping("/user")
public class SysUserController extends BaseController {

    @Resource
    private SysUserService sysUserService;

    /**
     * 跳转到登录页面
     * @return 登录页逻辑视图
     */
    @RequestMapping("/toLogin")
    public String toLogin() {
        return "user/login";
    }

    /**
     * 路由跳转
     * @return 跳转到超级管理员页面
     */
    @RequestMapping("admin")
    public String toAdmin() {
        return "user/admin/index";
    }

    /**
     * 路由跳转
     * @return 跳转到开发者页面
     */
    @RequestMapping("developer")
    public String toDeveloper() {
        return "user/developer/index";
    }

    /**
     * 用户登录
     * @param user 登录信息
     * @return 登录结果
     */
    @RequestMapping("/doLogin")
    public String doLogin(SysUser user, HttpSession session) {
        SysUser sysUser = sysUserService.login(user);
        if (sysUser != null) {
            session.setAttribute("sysUser", sysUser);
            //超级管理员
            if (sysUser.getUserRole() == 1) {
                return "redirect:admin";
            }//开发者
            else if (sysUser.getUserRole() == 2) {
                return "redirect:developer";
            }
        }
        return "redirect:toLogin";
    }

    /**
     * 退出登录
     * @param session session
     * @return 结果
     */
    @RequestMapping("/logOut")
    public String logOut(HttpSession session) {
        session.removeAttribute("sysUser");
        return "redirect:toLogin";
    }

    /**
     * 管理员跳转APP审核页面
     */
    @RequestMapping("/admin/appCheck")
    public String toAppCheck() {
        return "user/admin/appCheck";
    }
    /**
     * 开发者跳转到APP维护页面
     */
    @RequestMapping("/developer/appManage")
    public String toAppManage() {
        return "user/developer/appManage";
    }
}