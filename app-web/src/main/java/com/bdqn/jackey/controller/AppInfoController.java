package com.bdqn.jackey.controller;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.bdqn.jackey.entity.AppInfo;
import com.bdqn.jackey.service.AppInfoService;
import com.bdqn.jackey.utils.LayData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description：(AppInfo)表控制层
 * @author：jackey
 * @create：
 */
@Controller
@RequestMapping("/appInfo")
public class AppInfoController extends BaseController {

    @Resource
    private AppInfoService appInfoService;

    @RequestMapping("/list")
    @ResponseBody
    public String getAppInfo(AppInfo appInfo, @RequestParam("page") Integer page, @RequestParam("limit") Integer limit) {
        Integer count = appInfoService.queryAllCount(appInfo);
        List<AppInfo> infos = appInfoService.queryAllAppInfos(appInfo, page, limit);
        return JSONArray.toJSONString(LayData.getData(count, infos), SerializerFeature.WriteNullStringAsEmpty);
    }

    /**
     * APP logo上传
     *
     * @param file 文件信息
     * @return 上传结果
     * @throws Exception IO异常
     */
    @RequestMapping("/upload")
    @ResponseBody
    public LayData uploadLogo(MultipartFile file) throws Exception {
        /*
         System.currentTimeMillis()会一直根据时间戳刷新，所以必须声明变量来设置图片名，否则会导致
         上传到服务器的文件名和导入数据库的文件名不一致产生图片加载不到的问题
         ------因为上传时和修改数据库不是同是进行的
         */
        String fileName=System.currentTimeMillis() + file.getOriginalFilename();
        String realPath = "D:\\program\\Y2\\AppInfoSystem\\app-web\\src\\main\\webapp\\statics\\images\\" + fileName;
        FileUtil.writeBytes(file.getBytes(), realPath);
        return LayData.getUploadFile(fileName);
    }

    /**
     * 新增APP信息
     *
     * @param appInfo 封装信息的实体对象
     * @return 新增结果
     */
    @RequestMapping("/addSave")
    @ResponseBody
    public Integer addSave(AppInfo appInfo) {
        return appInfoService.insertSelective(appInfo);
    }

    /**
     * 修改app信息
     *
     * @param appInfo 封装提交信息的对象
     * @return 更新结果
     */
    @RequestMapping("/update")
    @ResponseBody
    public Integer updateSave(AppInfo appInfo) {
        return appInfoService.updateAppInfo(appInfo);
    }

    /**
     * 根据id删除信息
     *
     * @param id 主键
     * @return 删除结果
     */
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Integer deleteApp(@PathVariable("id") Long id) {
        return appInfoService.deleteAppInfoById(id);
    }

    /**
     * 根据id查询信息
     *
     * @param id 主键
     * @return 查询结果
     */
    @RequestMapping("/detail/{id}")
    @ResponseBody
    public AppInfo showDetail(@PathVariable("id") Long id) {
        return appInfoService.queryAppInfoById(id);
    }
}