package com.bdqn.jackey.controller;

import com.bdqn.jackey.service.SysRoleService;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @description：(SysRole)表控制层
 * @author：jackey
 * @create：
 */
@Controller
@RequestMapping("/sysRole")
public class SysRoleController extends BaseController{

    @Resource
    private SysRoleService sysRoleService;

}